Installation steps

1. Install visual studio with .Net 5 sdk.
2. Go to visual studio and then File > Clone Respository.
3. Give the repository location as "https://gitlab.com/sarthak_pandit/saas-product-inventory.git" and select the path to clone.

Follow the below steps to run the console application

1. Once cloned, go to Solution Explorer and double click on "SaasProductInventory.sln".
2. Run the console application by clicking the Play button representing "SaasProductInventory" on the top.
2. Follow the console window to import the data from different data source.

Follow the below steps to run the unit tests

1. Go to the Solution Explorer and then go inside "SaasProductInventory.Test" project.
2. Open the UnitTest.cs file.
3. To test any method go inside that method and do right click and select "Debug Test(s)".

Enchancements

1. We can build support for delete/update products.
2. We can directly fetch data from email for capterra and softwareadvice.
3. We can build progress bar for representing the status of import.
