﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaasProductInventory
{
    public class Constants
    {
        public readonly static string Capterra = "feed-products\\capterra.yaml";
        public readonly static string Softwareadvice = "feed-products\\softwareadvice.json";
        public readonly static string CsvDataSource = "";
    }
}
