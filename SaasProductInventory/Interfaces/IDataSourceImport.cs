﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaasProductInventory.Interfaces
{
    public interface IDataSourceImport
    {
        bool ImportCapterra();
        bool ImportSoftwareadvice();
        bool ImportCsvDataSource();

    }
}
