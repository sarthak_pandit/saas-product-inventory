﻿using Newtonsoft.Json;
using SaasProductInventory.Interfaces;
using SaasProductInventory.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace SaasProductInventory.Features.DataSourceImport
{
    public class DataSourceImport : IDataSourceImport
    {
        private readonly string _applicationPath;

        public DataSourceImport()
        {
            _applicationPath = System.IO.Directory.GetCurrentDirectory().Replace("\\bin\\Debug\\net5.0", "");

            //For unit test need to update directory
            if(_applicationPath.Contains("SaasProductInventory.Test"))
            {
                _applicationPath = _applicationPath.Replace("SaasProductInventory.Test", "SaasProductInventory");
            }
        }

        /// <summary>
        /// Import the Capterra data source which resides in yaml format
        /// </summary>
        public bool ImportCapterra()
        {
            string capterraSourcePath = Path.Combine(_applicationPath, Constants.Capterra);

            bool isImportSucceeded = false;

            try
            {
                //Setting up the deserializing rules 
                var deserializer = new DeserializerBuilder()
                .WithNamingConvention(CamelCaseNamingConvention.Instance)
                .Build();

                List<CapterraModel> capterraProducts = new List<CapterraModel>();

                //Reading and deserializing yaml file
                capterraProducts = deserializer.Deserialize<List<CapterraModel>>(File.OpenText(capterraSourcePath));

                string output = string.Empty;

                if (capterraProducts.Count > 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("````bash");
                    Console.WriteLine("$ import capterra feed-products/capterra.yaml\n");

                    foreach (CapterraModel product in capterraProducts)
                    {
                        // Doing null check assumming we don't want to print the null value field name
                        // If we want to print the field name in the case even if we have null value, we can use the below way
                        // product.Name?product.Name:' ';
                        if (product.Name != null)
                        {
                            output += $"importing: Name: \"{ product.Name}\"; ";
                        }
                        if (product.Tags != null)
                        {
                            output += "Categories: " + product.Tags + "; ";
                        }
                        if (product.Twitter != null)
                        {
                            output += "Twitter: @" + product.Twitter;
                        }
                        Console.WriteLine(output);
                        output = string.Empty;
                    }

                    Console.WriteLine("````");
                }
                else
                {
                    Console.WriteLine("No data available to import");
                }

                isImportSucceeded = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            return isImportSucceeded;
        }

        /// <summary>
        /// Import the Softwareadvice data source which resides in json format
        /// </summary>
        public bool ImportSoftwareadvice()
        {
            string softwareadviceSourcePath = Path.Combine(_applicationPath, Constants.Softwareadvice);

            bool isImportSucceeded = false;

            try
            {
                SoftwareadviceProductModel softwareadviceProducts = new SoftwareadviceProductModel();

                //Getting the json content  
                string Json = System.IO.File.ReadAllText(softwareadviceSourcePath);

                //Deserializing the json
                softwareadviceProducts = JsonConvert.DeserializeObject<SoftwareadviceProductModel>(Json);

                string output = string.Empty;

                if (softwareadviceProducts.Products.Count > 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("````bash");
                    Console.WriteLine("$ import capterra feed-products/softwareadvice.json\n");

                    foreach (SoftwareadviceModel product in softwareadviceProducts.Products)
                    {
                        // Doing null check assumming we don't want to print the null value field name
                        // If we want to print the field name in the case even if we have null value, we can use the below way
                        // product.Title?product.Title:' ';
                        if (product.Title != null)
                        {
                            output += $"importing: Name: \"{ product.Title}\"; ";
                        }
                        if (product.Categories != null)
                        {
                            output += "Categories: " + string.Join(',', product.Categories) + "; ";
                        }
                        if (product.Twitter != null)
                        {
                            output += "Twitter: " + product.Twitter;
                        }
                        Console.WriteLine(output);
                        output = string.Empty;
                    }

                    Console.WriteLine("````");
                }
                else
                {
                    Console.WriteLine("No data available to import");
                }

                isImportSucceeded = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            return isImportSucceeded;
        }

        public bool ImportCsvDataSource()
        {
            throw new NotImplementedException();
        }

    }
}
