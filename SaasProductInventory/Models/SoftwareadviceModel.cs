﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaasProductInventory.Models
{
    public class SoftwareadviceModel
    {
        public List<string> Categories { get; set; }
        public string Title { get; set; }
        public string Twitter { get; set; }
    }
}
