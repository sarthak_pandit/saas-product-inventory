﻿using SaasProductInventory.Features.DataSourceImport;
using SaasProductInventory.Interfaces;
using System;

namespace SaasProductInventory
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Select the Data source to import by giving its number");
            Console.WriteLine("1. Capterra");
            Console.WriteLine("2. Softwareadvice");

            string userSelection = Console.ReadLine();

            IDataSourceImport dataSource = new DataSourceImport();

            switch (userSelection)
            {
                case "1":
                    dataSource.ImportCapterra();
                    break;
                case "2":
                    dataSource.ImportSoftwareadvice();
                    break;
                default:
                    Console.WriteLine("Please select the correct data source");
                    break;
            }
        }
    }
}
