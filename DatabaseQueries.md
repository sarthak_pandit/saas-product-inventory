# SQL Test Assignment

Attached is a mysqldump of a database to be used during the test.

Below are the questions for this test. Please enter a full, complete, working SQL statement under each question. We do not want the answer to the question. We want the SQL command to derive the answer. We will copy/paste these commands to test the validity of the answer.

**Example:**

_Q. Select all users_

- Please return at least first_name and last_name

SELECT first_name, last_name FROM users;


------

**— Test Starts Here —**

1. Select users whose id is either 3,2 or 4
- Please return at least: all user fields

SELECT * FROM users WHERE id IN (2,3,4)

2. Count how many basic and premium listings each active user has
- Please return at least: first_name, last_name, basic, premium

SELECT usr.first_name, usr.last_name, (SELECT COUNT(lst.status) FROM listings lst WHERE lst.user_id=usr.id and lst.status=2) as basic,(select COUNT(lst.status) FROM listings lst WHERE lst.user_id=usr.id and lst.status=3) as premium FROM users usr WHERE usr.status=2 GROUP BY usr.first_name,usr.last_name, usr.id


3. Show the same count as before but only if they have at least ONE premium listing
- Please return at least: first_name, last_name, basic, premium

SELECT usr.first_name, usr.last_name, (SELECT COUNT(lst.status) FROM listings lst WHERE lst.user_id=usr.id and lst.status=2) as basic,(select COUNT(lst.status) FROM listings lst WHERE lst.user_id=usr.id and lst.status=3) as premium into #ListingInfo FROM users usr WHERE usr.status=2 GROUP BY usr.first_name,usr.last_name, usr.id
SELECT * FROM #ListingInfo WHERE premium>0
DROP TABLE #ListingInfo


4. How much revenue has each active vendor made in 2013
- Please return at least: first_name, last_name, currency, revenue

SELECT usr.first_name,usr.last_name, clk.currency, SUM(clk.price) as revenue FROM clicks clk JOIN listings lst ON clk.listing_id=lst.id 
JOIN users usr ON usr.id=lst.user_id where usr.status=2 and YEAR(clk.created)=2013 GROUP BY usr.first_name, usr.last_name, clk.currency, lst.user_id


5. Insert a new click for listing id 3, at $4.00
- Find out the id of this new click. Please return at least: id

INSERT INTO clicks(listing_id,price) VALUES (3,4.00)
Select @@IDENTITY as id  


6. Show listings that have not received a click in 2013
- Please return at least: listing_name

SELECT name as listing_name FROM listings WHERE id NOT IN(select listing_id FROM clicks WHERE YEAR(created)=2013)


7. For each year show number of listings clicked and number of vendors who owned these listings
- Please return at least: date, total_listings_clicked, total_vendors_affected

SELECT YEAR(clk.created) as year, Count(clk.id) as total_listings_clicked, Count(distinct usr.id) as total_vendors_affected FROM listings lst JOIN clicks clk ON lst.id=clk.listing_id
JOIN users usr ON usr.id=lst.user_id GROUP BY YEAR(clk.created)


8. Return a comma separated string of listing names for all active vendors
- Please return at least: first_name, last_name, listing_names

SELECT usr.first_name, usr.last_name,
STUFF((SELECT ', ' + lst.name
    FROM listings lst
    WHERE lst.user_id = usr.id
    FOR XML PATH('')), 1, 1, '') listing_names
FROM listings lst JOIN users usr ON lst.user_id=usr.id WHERE usr.status=2 GROUP BY usr.first_name,usr.last_name,usr.id