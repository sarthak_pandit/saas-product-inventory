using SaasProductInventory.Features.DataSourceImport;
using SaasProductInventory.Interfaces;
using System;
using Xunit;

namespace SaasProductInventory.Test
{
    public class UnitTest
    {
        private readonly IDataSourceImport _dataSourceImport;
        public UnitTest()
        {
            _dataSourceImport = new DataSourceImport();

        }
        /// <summary>
        /// Testing capterra dataSource
        /// </summary>
        [Fact]
        public void CapterraDataSource()
        {
            var isImported = _dataSourceImport.ImportCapterra();
            Assert.True(isImported);

        }

        /// <summary>
        /// Testing softwareadvice dataSource
        /// </summary>
        [Fact]
        public void SoftwareadviceDataSource()
        {
            var isImported = _dataSourceImport.ImportSoftwareadvice();
            Assert.True(isImported);
        }
    }
}
